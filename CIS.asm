; Code Injection Sample, 1.00
; Written by Lars Springintveld

.386
.model flat,stdcall

include windows.inc
include masm32.inc
include kernel32.inc

includelib masm32.lib
includelib kernel32.lib

.data

szCisDll			db "cisdll.dll",0
szKernel32		db "kernel32.dll",0
szLoadLib		db "LoadLibraryA",0
szSignal			db "CISAMPLE_EXE_FREE",0				; We will use this to create a mutex object to signal when this executable is no longer in use.
szTarget			db "calc.exe",0							; Windows calculator is our target in this example.

.code

CIS:

WinMain Proc hInstance:DWORD, hPrevInstance:DWORD, lpCmdLine:DWORD, nCmdShow:DWORD
	
	LOCAL dwBytesWritten:DWORD
	LOCAL dwCisDllPath:DWORD
	LOCAL dwCurrentDirectory:DWORD
	LOCAL dwHeapAllocationCount:DWORD
	LOCAL dwProcessHeap:DWORD
	LOCAL dwProcessInfo:DWORD
	LOCAL dwRemoteBuffer:DWORD
	LOCAL dwRemoteThreadHandle:DWORD
	LOCAL dwStartupInfo:DWORD
	LOCAL dwTargetFullPath:DWORD
	
	mov dwHeapAllocationCount, 0h						; Let's ensure this variable is empty because if it's not we're gonna be crashing severely at the end.
	
	call GetProcessHeap								; Get the default process heap's handle for later memory allocation.
	mov dwProcessHeap, eax
	
	push 0h
	push 0h
	call GetCurrentDirectory								; Get the required buffer size for our current directory string, including the terminating zero.
	push 0h
	push eax
	push eax
	push HEAP_ZERO_MEMORY
	push dwProcessHeap
	call HeapAlloc										; Allocate the buffer.
	inc dwHeapAllocationCount
	mov dwCurrentDirectory, eax
	mov [esp+4h], eax
	call GetCurrentDirectory								; Retrieve the current directory string.
	
	push MAX_PATH
	push HEAP_ZERO_MEMORY
	push dwProcessHeap
	call HeapAlloc										; Allocate a buffer for the system directory path string.
	inc dwHeapAllocationCount
	mov dwTargetFullPath, eax
	push MAX_PATH
	push eax
	call GetSystemDirectory								; Retrieve the system directory string; this is where we should find our target.
	push dwTargetFullPath
	call szLen											; Retrieve the length of the directory string.
	mov edx, SIZEOF szTarget
	inc edx
	add edx, eax
	.if edx > MAX_PATH									; If the buffer is not long enough for "calc.exe" to be appended to the system directory string (very little chance of that, but let's be certain...)
		push eax										; 	then we must create a buffer that is and copy our contents into the new buffer.
		push edx
		push HEAP_ZERO_MEMORY
		push dwProcessHeap
		call HeapAlloc
		mov ecx, [esp]
		mov esi, dwTargetFullPath
		mov edi, eax
		.while ecx										; We let the ECX register serve two purposes here:
			mov dl, [esi+ecx-1h]							; 	1) It functions as a counter for our loop, as which Intel intended the use of this register.
			mov [edi+ecx-1h], dl							; 	2) And it also functions as a displacement for our source and destination indexes in the copy process.
			dec ecx
		.endw
		mov dwTargetFullPath, eax						; Store our new buffer pointer...
		push esi
		push 0h
		push dwProcessHeap
		call HeapFree									; ...and free our old buffer.
		pop eax
	.endif
	mov edx, dwTargetFullPath
	add  edx, eax
	mov DWORD ptr [edx], 5Ch							; Add a backslash to the directory string...
	mov DWORD ptr [edx+1], 00h
	push OFFSET szTarget
	push dwTargetFullPath
	call szCatStr										; ...and then concatenate it with "calc.exe".
	
	; NOTE: I know I don't have to allocate memory for the following structures the way I'm doing and I could just define those variables as being of these structure types instead,
	; but I prefer to do it this way--don't ask why.
	push SIZEOF PROCESS_INFORMATION
	push HEAP_ZERO_MEMORY
	push dwProcessHeap
	call HeapAlloc										; Allocate memory for a PROCESS_INFORMATION structure.
	inc dwHeapAllocationCount
	mov dwProcessInfo, eax
	push SIZEOF STARTUPINFO
	push HEAP_ZERO_MEMORY
	push dwProcessHeap
	call HeapAlloc										; Allocate memory for a STARTUPINFO structure.
	inc dwHeapAllocationCount
	mov dwStartupInfo, eax

	push dwStartupInfo
	call GetStartupInfo									; Fill the STARTUPINFO structure with startup information of the current process.
	push dwProcessInfo
	push dwStartupInfo
	push dwCurrentDirectory								; I use this parameter to pass the directory in which "CIS.exe" resides into the target process.
	push 0h
	push CREATE_SUSPENDED
	push 0h
	push 0h
	push 0h
	push 0h
	push dwTargetFullPath
	call CreateProcess									; Attempt to launch Windows calculator.
	cmp eax, 0h
	je @F											; If the function failed skip to the end and close down.
	
	push dwCurrentDirectory
	call szLen
	sub esp, 4h
	push eax
	push eax
	inc eax
	add eax, SIZEOF szCisDll
	mov [esp+8h], eax
	push eax
	push HEAP_ZERO_MEMORY
	push dwProcessHeap
	call HeapAlloc										; Allocate memory for the full path to our Dynamic-Link Library.
	inc dwHeapAllocationCount
	mov dwCisDllPath, eax
	push eax
	push dwCurrentDirectory
	call MemCopy										; Copy the contents of the CurrentDirectory buffer, minus the string's terminating zero, into the DLL path buffer.
	pop edi
	mov edx, dwCisDllPath
	add edi, edx
	mov DWORD ptr [edi], 5Ch
	mov DWORD ptr [edi+1], 00h
	push OFFSET szCisDll
	push edx
	call szCatStr										; Concatenate the current directory string with its backslash just appended, and the name of our DLL.
	
	lea ebx, dwBytesWritten
	mov edx, [esp]
	mov [esp], ebx
	push edx
	push PAGE_READWRITE
	push MEM_COMMIT
	push edx
	push 0h
	mov edx, dwProcessInfo
	push (PROCESS_INFORMATION ptr [edx]).hProcess
	call VirtualAllocEx									; Allocate a buffer for our DLL path in the foreign process.
	mov dwRemoteBuffer, eax
	
	push dwCisDllPath
	push eax
	mov edx, dwProcessInfo
	push (PROCESS_INFORMATION ptr [edx]).hProcess
	call WriteProcessMemory								; Write the DLL path into the remote buffer.
	
	push OFFSET szSignal
	push 1h
	push 0h
	call CreateMutex									; Create a mutex object which the DLL will later wait for to become available.
													; 	We're not going to release this mutex though; Windows will clean it up the moment this process
													; 	terminates, making it an accurate indication of when the executable file from which the DLL was injected
													; 	is no longer in use and can be deleted.
	push OFFSET szKernel32
	call GetModuleHandle								; Retrieve the handle to the kernel32 module.
	push OFFSET szLoadLib
	push eax
	call GetProcAddress								; And then the offset of the LoadLibraryA API.
	
	push 0h
	push 0h
	push dwRemoteBuffer								; This is the parameter for LoadLibraryA.
	push eax											; And this the thread's start routine: LoadLibraryA.
	push 0h
	push 0h
	mov edx, dwProcessInfo
	push (PROCESS_INFORMATION ptr [edx]).hProcess
	call CreateRemoteThread							; There we go, the DLL should now be loaded, mapped and initialized.
	mov dwRemoteThreadHandle, eax
	
	push INFINITE
	push eax
	call WaitForSingleObject								; Keeping in mind the fact that we didn't set the current directory to its default, it's best to wait for the
													; 	DLL initialization to finish before resuming the process.
	mov edx, dwProcessInfo
	push (PROCESS_INFORMATION ptr [edx]).hThread
	call ResumeThread									; It is now definitely safe to let the target process' primary thread resume execution.
	
	push MEM_RELEASE
	push 0h
	push dwRemoteBuffer
	mov edx, dwProcessInfo
	push (PROCESS_INFORMATION ptr [edx]).hProcess
	call VirtualFreeEx									; Free the remote buffer.
	
	mov edx, dwProcessInfo
	push dwRemoteThreadHandle
	push (PROCESS_INFORMATION ptr [edx]).hThread
	push (PROCESS_INFORMATION ptr [edx]).hProcess
	call CloseHandle
	call CloseHandle									; Close all handles, apart--needless to say--from the one to our mutex object.
	call CloseHandle									; 	There's no way to 'accidentally' include the mutex' handle though, since we didn't save it.
	
	push dwCisDllPath
	@@:
	push dwStartupInfo
	push dwProcessInfo
	push dwTargetFullPath
	push dwCurrentDirectory
	.while dwHeapAllocationCount							; Free all previously allocated memory.
		sub esp, 8h
		mov DWORD ptr [esp+4], 0h
		mov edx, dwProcessHeap
		mov [esp], edx
		call HeapFree
		dec dwHeapAllocationCount
	.endw
	
	push 0h
	call ExitProcess
	
	ret
	
WinMain EndP

End CIS