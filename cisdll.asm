; Code Injection Sample, 1.00
; Written by Lars Springintveld

.386
.model flat,stdcall

include windows.inc
include user32.inc
include masm32.inc
include kernel32.inc

includelib user32.lib
includelib masm32.lib
includelib kernel32.lib

.data

szCisExe			db "CIS.exe",0
szSignal			db "CISAMPLE_EXE_FREE",0				; If we gain control of this mutex object, CIS.exe is no longer in use.
szTarget			db "calc.exe",0
szWndCaption		db "Calculator",0

.data?

dwCisData		DWORD ?
dwCisDataSize	DWORD ?
dwCisPath		DWORD ?
dwMutexHandle	DWORD ?
dwProcessHeap	DWORD ?
dwWindowProc	DWORD ?

.code

CISDLL:

DllMain Proc hinstDLL:DWORD, fdwReason:DWORD, lpvReserved:DWORD

	.if fdwReason == DLL_PROCESS_ATTACH
	
		push OFFSET szSignal
		push 0h
		push 1F0001h
		call OpenMutex
		mov dwMutexHandle, eax
		
		call GetProcessHeap
		mov dwProcessHeap, eax
		
		push 0h
		push 0h
		call GetCurrentDirectory
		add eax, SIZEOF szCisExe
		push eax
		push eax
		push HEAP_ZERO_MEMORY
		push dwProcessHeap
		call HeapAlloc
		mov  dwCisPath, eax
		mov edx, [esp]
		mov [esp], eax
		push edx
		call GetCurrentDirectory							; We have now saved the directory string indicating where CIS.exe resides.
		
		push MAX_PATH
		push HEAP_ZERO_MEMORY
		push dwProcessHeap
		call HeapAlloc									; Allocate memory for a temporary buffer.
		push eax
		push OFFSET szTarget
		call GetModuleHandle							; Get the handle of the "calc.exe" module in the current process.
		push MAX_PATH
		push [esp+4h]
		push eax
		call GetModuleFileName							; Retrieve the path to "calc.exe".
		push [esp]
		call szLen										; Measure the length of the string in our buffer.
		sub eax, SIZEOF szTarget
		mov edi, [esp]
		xchg eax, edi
		add edi, eax
		mov BYTE ptr [edi], 00h
		push eax
		call SetCurrentDirectory							; Set the correct current directory.
		push 0h
		push dwProcessHeap
		call HeapFree									; Free the temporary buffer.
		
		; NOTE: Everything we've now done regarding the use of the lpCurrentDirectory parameter in CreateProcess(10) as a method of passing a parameter from CIS.exe
		; into our Dynamic-Link Library cisdll.dll is essentially useless in this particular case.
		; I did this, rather than simply calling GetModuleFileName(3) here with the handle of "cisdll.dll" instead of "calc.exe" as the hModule parameter, merely
		; for the sake of example.
		; There are, after all, other situations in which this can be useful and now that you've see it in action here you should be able to apply it elsewhere if needed.
		; 
		; The same goes for creating the "calc.exe" process in a suspended state to prevent it from suicidal code execution using your altered current directory string in code
		; that cannot cope with it.
		; Windows calculator won't be bothered by its current directory setting no matter what you set it to, but there are other applications that are a lot more picky about it and
		; this example aims to show you how to deal with them.
		
		push 0h
		push 0h
		push 0h
		push DllRoutines
		push 0h
		push 0h
		call CreateThread								; Continuing our work on a new thread allows this thread to let LoadLibrary return and thus for our executable
													; 	to also resume execution.
	.endif											;	Apart from that we also need the CIS.exe process to terminate before deleting its source file from here.
	
	ret
	
DllMain EndP

DllRoutines Proc lpParameter:DWORD

	LOCAL dwBytesRead:DWORD
	LOCAL dwFileHandle:DWORD
	LOCAL dwWindowHandle:DWORD

	push dwCisPath
	call szLen
	mov edx, dwCisPath
	mov cx, 5C00h
	mov [edx+eax], ch
	mov [edx+eax+1], cl
	push OFFSET szCisExe
	push dwCisPath
	call szCatStr										; Concatenate the directory of CIS.exe, with the backslash we just appended, and the actual executable name.
	
	push INFINITE
	push dwMutexHandle
	call WaitForSingleObject								; Wait for ownership of the mutex.
													; 	This function will return with WAIT_ABANDONED which indicates that the thread that had ownership of the
													; 	mutex object before did not release it before it terminated and with that also that it did terminate.
	push dwMutexHandle
	call CloseHandle									; Close the mutex.
	
	push 0h
	push FILE_ATTRIBUTE_NORMAL
	push OPEN_EXISTING
	push 0h
	push 0h
	push FILE_READ_DATA
	push dwCisPath
	call CreateFile										; Open CIS.exe with read access.
	mov dwFileHandle, eax
	push 0h
	push eax
	call GetFileSize									; Retrieve the filesize.
	mov dwCisDataSize, eax
	push PAGE_READWRITE
	push MEM_COMMIT
	push eax
	push 0h
	call VirtualAlloc										; Allocate a buffer to store the file in.
	mov dwCisData, eax
	push 0h
	lea edx, dwBytesRead
	push edx
	push dwCisDataSize
	push eax
	push dwFileHandle
	call ReadFile										; Read the data.
	push dwFileHandle
	call CloseHandle									; Close CIS.exe.
	
	push OFFSET szWndCaption
	push 0h
	call FindWindow									; Retrieve a handle to Windows calculator's main window.
	cmp eax, 0h
	je @F
	mov dwWindowHandle, eax
	push WindowProc
	push GWL_WNDPROC
	push eax
	call SetWindowLong								; Set our window procedure to be the default one.
	cmp eax, 0h
	je @F
	mov dwWindowProc, eax
	push dwWindowHandle
	call CloseHandle									; Close the handle to the window.
	
	push dwCisPath
	call DeleteFile										; If we reach this point the window procedure has been altered successfully and it is now safe to delete CIS.exe.
	
	ret

	@@:	
	cmp dwWindowHandle, 0h
	je @F
	push dwWindowHandle
	call CloseHandle
	@@:
	call PrepareShutdown								; If we failed to change the window procedure we shall prepare for shutdown.
													; 	Normally I'd implement back-up procedures and all but that's beyond the scope of this particular example.
	ret

DllRoutines EndP

PrepareShutdown Proc

	; All we need to do here is free the following two previously allocated memory buffers:
	; 	1) The buffer which holds the contents of CIS.exe.
	; 	2) The bufffer for CIS.exe's full pathname which resides in one of the current process' heaps.
	
	push MEM_RELEASE
	push 0h
	push dwCisData
	call VirtualFree
	
	push dwCisPath
	push 0h
	push dwProcessHeap
	call HeapFree
	
	ret
	
PrepareShutdown EndP

WindowProc Proc hwnd:DWORD, uMsg:DWORD, wParam:DWORD, lParam:DWORD

	LOCAL dwBytesWritten:DWORD

	.if uMsg == WM_DESTROY							; If we receive the WM_DESTROY message it's about time to recreate CIS.exe.
	
		push 0h
		push FILE_ATTRIBUTE_NORMAL
		push CREATE_ALWAYS
		push 0h
		push 0h
		push FILE_WRITE_DATA
		push dwCisPath
		call CreateFile									; Create a new CIS.exe file with write access.
		push eax
		push 0h
		lea edx, dwBytesWritten
		push edx
		push dwCisDataSize
		push dwCisData
		push eax
		call WriteFile									; Write the original contents of CIS.exe into the file...
		call CloseHandle								; ...and close it.
		
		call PrepareShutdown							; Here we'll perform some final cleanup.
	
	.endif
	
	push lParam
	push wParam
	push uMsg
	push hwnd
	push dwWindowProc
	call CallWindowProc								; After examining the message and, if needed, having taken action in response to it we must call the original
													; 	window procedure function to do the actual processing that the application requires.
	ret
	
WindowProc EndP

End CISDLL